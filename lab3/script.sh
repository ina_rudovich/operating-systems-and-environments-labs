#!/bin/bash

lower_case=( a b c d e f g h i j k l m n o p q r s t u v w x y z )

upper_case=( A B C D E F G H I J K L M N O P Q R S T U V W X Y Z )

for i in {0..25}
do
    regex=':a;N;$!ba;s/\([\!\.\?][ \t\n]*\)\('${lower_case[$i]}'\)/\1'${upper_case[$i]}'/g;'
    sed -i "$regex" $1
    regex='1s/\(^[ \t\n$]*\)\('${lower_case[$i]}'\)/\1'${upper_case[$i]}'/g;'
    sed -i "$regex" $1
done

cat $1
