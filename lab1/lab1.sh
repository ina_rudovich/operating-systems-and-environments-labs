#!/bin/bash


current_time=`date +"%s"`
# div_time=`date +"%s" -d"$2"`
div_time=$2

possible_time=`expr $current_time - $div_time`
files=`find $1`

for file in $files
do
    file_time=`date +"%s" --reference="$file"`
    if [[ $file_time < $possible_time ]]
    then
        echo "$file"
    fi
done
